# flaskproto
A flask prototype application to get started quickly

needs python3, node, yarn and sass

Within the flaskproto project root directory:

Linux
-----

### create virtual environment
```shell
$ python -m venv venv
```

### and activate it
```shell
$ . venv/bin/activate
```

### setup database
```shell
$ python
>>> from flaskproto import db
>>> from flaskproto.models import User, Post
>>> db.create_all()
>>> exit()
```

### install client dependencies
```shell
$ yarn install
```

### build client
```shell
$ python build.py
```

### run the app
```shell
$ export FLASK_ENV=development
$ python run.py
```

(on german linux dist i also had to)
http://click.pocoo.org/5/python3/#unicode-literals
```shell
$ export LC_ALL=de_DE.utf-8
$ export LANG=de_DE.utf-8
```
