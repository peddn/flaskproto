import os
import secrets

from PIL import Image

from flask import render_template, url_for, flash, redirect, request, jsonify
from flask_login import login_user, logout_user, current_user, login_required

from marshmallow import pprint, ValidationError

from flaskproto import app, db, bcrypt
from flaskproto.models import User, user_schema, users_schema, Post, post_schema, posts_schema
from flaskproto.forms import RegistrationForm, LoginForm, UpdateAccountForm
from flaskproto.api_errors import InvalidUsage


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html', title='About flaskproto')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created.', 'flash-success')
        flash('You are now able to log in.', 'flash-success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=False)
            next_page = request.args.get('next')
            flash('You are now logged in.', 'flash-success')
            if next_page:
                return redirect(url_for(next_page[1:]))
            else:
                return redirect(url_for('index'))
        else:
            flash('Login was not successful. Please check email and password.', 'flash-error')
    return render_template('login.html', title='Login', form=form)

@app.route('/logout')
def logout():
    logout_user()
    flash('You are now logged out.', 'flash-success')
    return redirect(url_for('index'))

def save_account_picture(picture):
    url_token = secrets.token_urlsafe(16)
    _, extension = os.path.splitext(picture.filename)
    picture_filename = url_token + extension
    picture_path = os.path.join(app.root_path, 'static', 'img', 'profile-pictures', picture_filename)
    if current_user.image_file != 'default.png':
        old_file = os.path.join(app.root_path, 'static', 'img', 'profile-pictures', current_user.image_file)
        os.remove(old_file)
    output_size = (200, 200)
    image = Image.open(picture)
    image.thumbnail(output_size)
    image.save(picture_path)
    return picture_filename

@app.route('/account', methods=['GET', 'POST'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_filename = save_account_picture(form.picture.data)
            current_user.image_file = picture_filename
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('You have successfully updated yout account.', 'flash-success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    return render_template('account.html', title='your account', form=form)

@app.route('/posts')
@login_required
def posts():
    return render_template('posts.html', title='manage your posts')


@app.route('/api/post/create', methods=['POST'])
@login_required
def post_create():
    json_post = request.get_json()
    if not json_post:
        raise InvalidUsage('No data provided.', status_code=400)
    # Validate and deserialize input
    data = post_schema.load(json_post)
    if not data.errors:
        post_data = data.data
        post = Post(
            title = post_data['title'],
            content = post_data['content'],
            user_id = current_user.id
        )
        db.session.add(post)
        db.session.commit()
        created_post = Post.query.get(post.id)
        created_post_dumped = post_schema.dump(created_post)
        return jsonify({
            'message': 'Post created.',
            'post': created_post_dumped.data
        })
    else:
        #print(data.errors)
        raise InvalidUsage('Bad data provided.', status_code=422)

@app.route('/api/post/retrieve', methods=['GET'])
@login_required
def post_retrieve():
    posts = Post.query.all()
    # Serialize the queryset
    posts_dumped = posts_schema.dump(posts)
    if not posts_dumped.errors:
        return jsonify({
            'message': 'Posts retireved.',
            'posts': posts_dumped.data
        })
    else:
        #print(posts_dumped.errors)
        raise InvalidUsage('Internal Error.', status_code=500)

@app.route('/api/posts/update', methods=['PUT'])
@login_required
def post_update():
    json_post = request.get_json()
    if not json_post:
        raise InvalidUsage('No data provided.', status_code=400)
    # Validate and deserialize input
    data = post_schema.load(json_post)
    if not data.errors:
        post_data = data.data
        post = Post.query.get(json_post['id'])
        post.title = post_data['title']
        post.content = post_data['content']
        db.session.commit()
        created_post = Post.query.get(post.id)
        created_post_dumped = post_schema.dump(created_post)
        return jsonify({
            'message': 'Post updated.',
            'post': created_post_dumped.data
        })
    else:
        #print(data.errors)
        raise InvalidUsage('Bad data provided.', status_code=422)

@app.route('/api/posts/delete', methods=['DELETE'])
@login_required
def post_delete():
    json_post = request.get_json()
    if not json_post:
        raise InvalidUsage('No data provided.', status_code=400)
    # Validate and deserialize input
    data = post_schema.load(json_post)
    if not data.errors:
        post = Post.query.get(json_post['id'])
        deleted_post_dumped = post_schema.dump(post)
        db.session.delete(post)
        db.session.commit()
        return jsonify({
            'message': 'Post deleted.',
            'post': deleted_post_dumped.data
        })
    else:
        #print(data.errors)
        raise InvalidUsage('Bad data provided.', status_code=422)
