import M from 'materialize-css';

class Global {

    static createIcon(type) {
        let icon = document.createElement("i");
        icon.classList.add('material-icons');
        let text = document.createTextNode(type);
        icon.appendChild(text);
        console.log(icon);
        return icon;
    }


    
    static createFlashes() {
        let flashes = document.querySelectorAll('.flash');
        for(let flash of flashes) {
            // remove flash from the DOM and wrap it with jQuery
            flash = flash.parentNode.removeChild(flash);
            flash.classList.add('valign-wrapper')
            // prepend the corresponding icon to the flash according to the flash category 
            if(flash.classList.contains('flash-success')) {
                let icon = this.createIcon('done');
                flash.prepend(icon);
            }
            if(flash.classList.contains('flash-warning')) {
                let icon = this.createIcon('warning');
                flash.prepend(icon);
            }
            if(flash.classList.contains('flash-error')) {
                let icon = this.createIcon('error');
                flash.prepend(icon);
            }
            M.toast({html: flash.outerHTML});
        }
    }
    
    static initSidenav() {
        let sidenavTrigger = document.getElementById('sidenav-trigger');
        if(sidenavTrigger) {
            let sidenavElement = document.getElementById('sidenav-account');
            let sidenav = M.Sidenav.init(sidenavElement, { edge: 'right'});
            sidenavTrigger.addEventListener("click", (event) => {
                if(sidenav.isOpen) {
                    sidenav.close();
                } else {
                    sidenav.open();
                }
            })
        }
    }
    
    static initModalChat() {
        let modalChat = document.getElementById('modal-chat');
        M.Modal.init(modalChat, {});
    }

    static initButtonUser() {
        let buttonUser = document.getElementById('button-user');
        M.FloatingActionButton.init(buttonUser, {});
    }

    static create() {
        this.createFlashes();
        this.initSidenav();
        this.initModalChat();
        this.initButtonUser();
    }

    static generateRandomToken() {
        let tokenArray = new Uint32Array(2);
        window.crypto.getRandomValues(tokenArray);
        let token = tokenArray.join('');
        return token;
    }

    static formatDate(dateString) {
        let date = new Date(dateString);
        // Langer Wochentag mit langem Datum
        var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        let dateFormated = new Intl.DateTimeFormat('de-DE', options).format(date);
        return dateFormated.toString();
    }
}

export default Global;

