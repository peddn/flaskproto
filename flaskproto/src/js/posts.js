import M from 'materialize-css';
import Vue from 'vue';

import Global from './lib/Global.js';

import store from './store/store.js';

import Posts from './components/apps/posts/Posts.vue';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({
        el: '#app',
        mounted: function() {
            M.updateTextFields();
        },
        store,
        components: {
            Posts
        },
        template: `
            <posts></posts>
        `
    });
    Global.create();
});
