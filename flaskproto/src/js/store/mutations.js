export default {
    resetElements: (state) => {
        state.elements = [];
    },
    retrieveElements: (state, elements) => {
        state.elements = elements;
    },
    createElement: (state, element) => {
        state.elements.push(element);
    },
    updateElement: (state, element) => {
        let elementIndex = state.elements.indexOf(element.original);
        state.elements.splice(elementIndex, 1, element.updated);
    },
    deleteElement: (state, element) => {
        let elementIndex = state.elements.indexOf(element);
        state.elements.splice(elementIndex, 1);
    },
    setPage: (state, page) => {
        state.page = page;
    },
    increasePage: (state) => {
        state.page++;
    },
    decreasePage: (state) => {
        state.page--;
    },
    setElementsPerPage: (state, elementsPerPage) => {
        state.elementsPerPage = elementsPerPage;
    }
}