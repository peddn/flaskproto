import Vue from 'vue';
import Vuex from 'vuex';

import getters from './getters.js';
import actions from './actions.js';
import mutations from './mutations.js';

Vue.use(Vuex);

// Singleton export
export default new Vuex.Store({
    state: {
        elements: [],
        page: 1,
        elementsPerPage: 5
    },
    getters,
    mutations,
    actions
});