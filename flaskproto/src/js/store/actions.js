import PostsApi from '../api/posts.js';

// these actions are specific for any used api

export default {
    // { commit, state, getter }, data
    async createPost({ commit }, post) {
        try {
            let result = await PostsApi.create(post);
            commit('createElement', result.post);
            return {
                success: true,
                message: result.message
            };
        } catch (error) {
            return {
                success: false,
                message: error.message
            };
        }
    },
    async retrievePosts({ commit}) {
        try {
            let result = await PostsApi.retrieve();
            commit('retrieveElements', result.posts);
            return {
                success: true,
                message: result.message
            };

        } catch (error) {
            return {
                success: true,
                message: errror.message
            };
        }
    },
    async updatePost({ commit }, post) {
        try {
            let result = await PostsApi.update(post.updated);
            commit('updateElement', {
                original: post.original,
                updated: result.post
            });
            return {
                success: true,
                message: result.message
            };

        } catch (error) {
            return {
                success: true,
                message: error.message
            };
        }
    },
    async deletePost({ commit }, post) {
        try {
            let result = await PostsApi.delete(post);
            console.log('result', result);
            commit('deleteElement', post);
            return {
                success: true,
                message: result.message
            };
        } catch (error) {
            return {
                success: false,
                message: error.message
            };
        }
    },
}