export default {
    getElementById: (state) => (id) => {
        return state.elements.find(element => element.id === id);
    },
    elementsCount: (state) => {
        return state.elements.length;
    },
    pagesCount: (state, getters) => {
        return Math.ceil(getters.elementsCount / state.elementsPerPage);
    },
    elementsForPage: (state, getters) => {
        return state.elements.slice(state.elementsPerPage * (state.page - 1), (state.elementsPerPage * state.page));
    }
}