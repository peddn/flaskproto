class PostsApi {
    static async create(post) {
        let response = await fetch('/api/post/create', {
            method: 'POST',
            body: JSON.stringify(post),
            headers:{
                'Content-Type': 'application/json'
            }
        });
        if(!response.ok) {
            let error = await response.json();
            throw new Error(error.message);
        } else {
            let result = await response.json();
            return result;
        }
    }

    static async retrieve() {
        let response = await fetch('/api/post/retrieve', { method: 'GET'});
        if (!response.ok) {
            let error = await response.json();
            throw new Error(error.message);
        } else {
            let result = await response.json();
            return result;
        }
    }

    static async update(post) {
        let response = await fetch('/api/posts/update', {
            method: 'PUT',
            body: JSON.stringify(post),
            headers:{
                'Content-Type': 'application/json'
            }
        });
        if(!response.ok) {
            let error = await response.json();
            throw new Error(error.message);
        } else {
            let result = await response.json();
            return result;
        }
    }

    static async delete(post) {
        let response = await fetch('/api/posts/delete', {
            method: 'DELETE',
            body: JSON.stringify(post),
            headers:{
                'Content-Type': 'application/json'
            }
        });
        if(!response.ok) {
            let error = await response.json();
            throw new Error(error.message);
        } else {
            let result = await response.json();
            return result;
        }
    }
}

export default PostsApi;
