const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin')


module.exports = {
    mode: 'development',
    //mode: 'production',
    entry: {
        app: path.resolve(__dirname, 'src', 'js', 'app.js'),
        posts: path.resolve(__dirname, 'src', 'js', 'posts.js')
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'static', 'js')
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js' // Use the full build
        }
    },
    optimization: {
        splitChunks: {
            automaticNameDelimiter: '-',
            chunks: 'all',
            cacheGroups: {
                commons: {
                    name: 'commons',
                    chunks: 'initial',
                    minChunks: 2
                }
            }
        }
    },
    module: {
        rules: [
            {
              test: /\.vue$/,
              loader: 'vue-loader'
            }
        ]
    },
    plugins: [
        // make sure to include the plugin for the magic
        new VueLoaderPlugin()
    ]
};