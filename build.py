import os
import subprocess
import shutil

project = 'flaskproto'

root_path = os.getcwd()
src_path = os.path.join(root_path, project, 'src')
static_path = os.path.join(root_path, project, 'static')
node_modules_path = os.path.join(root_path, 'node_modules')

def get_materialize_sass_loadpaths():
    materialize_path = os.path.join(node_modules_path, 'materialize-css')
    return [ os.path.join(materialize_path, 'sass'), os.path.join(materialize_path, 'sass', 'components'), os.path.join(materialize_path, 'sass', 'components', 'forms')]

def task_success_message(message):
    return '[TASK-SUCCESS] ' + message

def task_error_message(message):
    return '[TASK-ERROR] ' + message
    
def clean_static_dir():
    try:
        shutil.rmtree(static_path)
    except FileNotFoundError as error:
        print(task_error_message(' '.join( [ error.strerror, ' - while deleting static dir', static_path ] )))
    try:
        os.mkdir(static_path)
    except FileExistsError as error:
        print(task_error_message(' '.join( [ error.strerror, ' - while making static dir', static_path ] )))
    print(task_success_message('Static directory cleaned'))
    
def copy_source_dir(src_dir):
    src = os.path.join(src_path, src_dir)
    dest = os.path.join(static_path, src_dir)
    shutil.copytree(src, dest)
    print(task_success_message('Source directory copied'))

def run_sass(input_file, output_file):
    command_list = [ 'sass' ]
    for load_path in get_materialize_sass_loadpaths():
        command_list.extend( [ '--load-path', load_path ] )
    command_list.extend([ input_file, output_file ])
    try:
        # TODO: find a solution for not using shell=True because of
        # https://docs.python.org/3/library/subprocess.html#frequently-used-arguments
        subprocess.check_call(command_list, shell=True)
        print(task_success_message(' '.join(command_list)))
    except subprocess.CalledProcessError as error:
        print(task_error_message(' '.join(command_list)))
        print(error)
    except OSError as error:
        print(task_error_message(' '.join(command_list)))
        print(error)

def run_webpack():
    command_list = [ 'npx', 'webpack' ]
    command_list.extend( [ '--config', os.path.join(root_path, project, 'webpack.config.js') ] )
    try:
        # TODO: find a solution for not using shell=True because of
        # https://docs.python.org/3/library/subprocess.html#frequently-used-arguments
        subprocess.check_call(command_list, shell=True)
        print(task_success_message(' '.join(command_list)))
    except subprocess.CalledProcessError as error:
        print(task_error_message(' '.join(command_list)))
        print(error)
    except OSError as error:
        print(task_error_message(' '.join(command_list)))
        print(error)


clean_static_dir()
copy_source_dir('data')
copy_source_dir('img')
copy_source_dir('assets')

input_file = os.path.join(src_path, 'sass', 'style.scss')
output_file = os.path.join(static_path, 'css', 'style.css')
run_sass(input_file, output_file)
run_webpack()